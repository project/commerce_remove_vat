INTRODUCTION
------------
The commerce remove cart vat tax module provide functionality to show original
product price and amount total from cart page.

* By default vat tax are included with price and total in cart page.
* Remove vat tax from cart page and show original price.

REQUIREMENTS
------------

Install Drupal Commerce and enable tax module.

INSTALLATION
------------

* Install as you would normally install  
a contributed Drupal module.


CONFIGURATION
------------
No Need of Configuration.

MAINTAINERS
-----------

Current maintainers:
* Sumit kumar (https://www.drupal.org/u/bsumit5577)
